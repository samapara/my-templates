let navLinks = document.getElementById('nav-links');
let navbarToggle = document.getElementById('navbar-toggle');

navbarToggle.addEventListener('click', () => {
    navLinks.classList.toggle('active');
})